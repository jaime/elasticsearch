package middleware_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/jaime/elasticsearch/server/internal/middleware"
)

func TestLogRequest(t *testing.T) {
	next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	logger, hook := test.NewNullLogger()

	handler := middleware.LogRequest(logger)(next)

	ww := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "http://localhost:8080/path?query=q", nil)

	handler.ServeHTTP(ww, r)

	res := ww.Result()
	defer res.Body.Close()

	entry := hook.LastEntry()
	require.NotNil(t, entry)

	assert.Equal(t, entry.Message, "access request")

	data := entry.Data
	require.NotNil(t, data)

	assert.Equal(t, data["req_host"], r.Host)
	assert.Equal(t, data["req_uri"], r.RequestURI)
	assert.Equal(t, data["req_method"], r.Method)
	assert.Equal(t, data["res_status"], res.StatusCode)
}

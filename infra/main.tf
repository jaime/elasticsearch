terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 1.0.5"
}

variable "region" {
  default = "ap-southeast-2"
}

provider "aws" {
  profile = "default"
  region  = var.region
}

resource "tls_private_key" "ecs_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

module "key_pair" {
  source          = "terraform-aws-modules/key-pair/aws"
  create_key_pair = true

  key_name   = "ecs_key"
  public_key = tls_private_key.ecs_key.public_key_openssh
}

resource "aws_iam_role" "ecs_instance_role" {
  name = "ecs_instance_role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Effect": "Allow",
        "Principal": {
            "Service": "ec2.amazonaws.com"
        }
    }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_instance_role" {
  name = "ecs_instance_role"
  role = aws_iam_role.ecs_instance_role.name
}


resource "aws_instance" "ecs_main" {
  ami                  = "ami-02febe6ca15caa2db" # ECS optimized Sydney Region
  instance_type        = "t2.micro"
  key_name             = "ecs_key"
  user_data            = file("instance_user_data.sh")
  iam_instance_profile = aws_iam_instance_profile.ecs_instance_role.name
  tags = {
    Name = "ECSElasticsearchZip"
  }
}

resource "aws_cloudwatch_log_group" "ecs_log_group" {
  name              = "ecs_log_group"
  retention_in_days = 1
}

resource "aws_ecs_cluster" "zip_elasticsearch" {
  name = "zip_elasticsearch"

  configuration {
    execute_command_configuration {
      logging = "OVERRIDE"

      log_configuration {
        cloud_watch_encryption_enabled = true
        cloud_watch_log_group_name     = aws_cloudwatch_log_group.ecs_log_group.name
      }
    }
  }
}
data "aws_iam_policy_document" "task-assume-role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "ecsTaskExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.task-assume-role.json
}
resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_ecs_task_definition" "elasticsearch_service" {
  family             = "elasticsearch_service"
  execution_role_arn = aws_iam_role.ecsTaskExecutionRole.arn
  container_definitions = jsonencode([
    {
      name  = "elasticsearch"
      image = "docker.elastic.co/elasticsearch/elasticsearch:7.14.0"
      environment = [
        { name : "discovery.type", value : "single-node" }
      ]
      cpu       = 10
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 9200
          hostPort      = 9200
        }
      ]
    }
  ])

  volume {
    name      = "elasticsearch-storage"
    host_path = "/ecs/service-storage"
  }
}

resource "aws_ecs_service" "elasticsearch_service" {
  name            = "elasticsearch_db"
  cluster         = aws_ecs_cluster.zip_elasticsearch.id
  task_definition = aws_ecs_task_definition.elasticsearch_service.arn
  desired_count   = 1

  ordered_placement_strategy {
    type  = "binpack"
    field = "cpu"
  }
}

resource "aws_ecr_repository" "zip_go_server" {
  name                 = "zip/server"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository" "zip_python_client" {
  name                 = "zip/client"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository" "zip_python_lambda" {
  name                 = "zip/lambda"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}


resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

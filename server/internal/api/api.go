package api

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
)

// User model.
type User struct {
	ID int64 `json:"id"`
}

// Storage defines the set of actions required by the API.
type Storage interface {
	Save(context.Context, *User) error
}

// API defines the handlers available to the HTTP server.
// It uses a Storage to save Users.
type API struct {
	log         logrus.FieldLogger
	store       Storage
	saveTimeout time.Duration
}

// New creates an API with a given Storage.
func New(logger logrus.FieldLogger, store Storage, saveTimeout time.Duration) *API {
	return &API{
		log:         logger,
		store:       store,
		saveTimeout: saveTimeout,
	}
}

// Click expects a ?user_id=123 to be a number and to exists.
// Returns 204 on successfully saving a User in the configured storage.
func (a *API) Click(w http.ResponseWriter, r *http.Request) {
	_ = r.ParseForm()

	userIDStr := r.FormValue("user_id")
	if userIDStr == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("missing query param: user_id\n"))

		return
	}

	userID, err := strconv.ParseInt(userIDStr, 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("query param: user_id must be an integer\n"))

		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), a.saveTimeout)
	defer cancel()

	if err := a.store.Save(
		ctx,
		&User{
			ID: userID,
		}); err != nil {
		a.log.WithError(err).Error("saving user")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("something went wrong\n"))

		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// HealthCheck handler
func (a *API) HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

// NotAllowed handler overrides the default gorilla/mux handler so it can be logged.
func (a *API) NotAllowed(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
}

// NotFound handler overrides the default gorilla/mux handler so it can be logged.
func (a *API) NotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
}

package middleware

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

type customRecorder struct {
	http.ResponseWriter
	status int
}

func (r *customRecorder) WriteHeader(status int) {
	r.status = status
	r.ResponseWriter.WriteHeader(status)
}

// LogRequest middleware uses a customRecorder to save the respsonse status code
// and log the request information.
func LogRequest(logger logrus.FieldLogger) func(http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			recorder := &customRecorder{
				ResponseWriter: w,
			}

			handler.ServeHTTP(recorder, r)

			logger.WithFields(logrus.Fields{
				"remote_address": r.RemoteAddr,
				"referrer":       r.Referer(),
				"content_length": r.ContentLength,
				"content_type":   r.Header.Get("Content-Type"),
				"user_agent":     r.UserAgent(),
				"req_host":       r.Host,
				"req_uri":        r.RequestURI,
				"req_method":     r.Method,
				"res_status":     recorder.status,
				"res_headers":    recorder.Header(),
			}).Info("access request")
		})
	}
}

package config

import "time"

type Config struct {
	Server        *Server
	ElasticServer *ElasticServer
}

type Server struct {
	Addr             string
	OperationTimeout time.Duration
}

type ElasticServer struct {
	URL       string
	IndexName string
}

func New() *Config {
	initFlags()

	return &Config{
		Server: &Server{
			OperationTimeout: *operationTimeout,
			Addr:             *address,
		},
		ElasticServer: &ElasticServer{
			URL:       *esServerURL,
			IndexName: *esIndexName,
		},
	}
}

"""
simple client making calls to the clicks API
"""

import os
import time
from random import randrange
import requests
import schedule
import structlog
from structlog.processors import JSONRenderer, TimeStamper

log = structlog.get_logger(
    processors=[TimeStamper(fmt='iso'),JSONRenderer( sort_keys=True)])

def click():
    """
    click makes PUT requests to /api/v1/click with a random user_id
    """

    server_address = os.getenv("SERVER_ADDRESS")
    max_range_str = os.getenv("MAX_USER_IDS")
    max_range = 10
    if max_range_str is not None:
        max_range = int(max_range_str)

    user_id = randrange(1, max_range)

    res = requests.put('{}/api/v1/click?user_id={}'.format(server_address, user_id))
    if res.status_code != 204:
        log.error("request failed", status_code= res.status_code, user_id = user_id)
        return

    log.info("sent request", user_id= user_id, status_code = res.status_code)

schedule.every(1).seconds.do(click)

while 1:
    schedule.run_pending()
    time.sleep(1)

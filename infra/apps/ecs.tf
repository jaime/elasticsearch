terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 1.0.5"
}

variable "region" {
  default = "ap-southeast-2"
}

provider "aws" {
  profile = "default"
  region  = var.region
}

data "aws_instance" "ecs_main" {
  # TODO: I believe this will retrieve all instances with the same tag? If so, I should use the ID
  # result from `infra/main.tf`
  instance_tags =  {
    Name = "ECSElasticsearchZip"
  }
}

data "aws_iam_role" "ecsTaskExecutionRole" {
  name = "ecsTaskExecutionRole"
}

data "aws_ecr_repository" "zip_go_server" {
  name = "zip/server"
}
data "aws_ecr_repository" "zip_python_client" {
  name = "zip/client"
}

data "aws_cloudwatch_log_group" "ecs_log_group" {
  name = "ecs_log_group"
}

data "aws_ecs_cluster" "zip_elasticsearch" {
  cluster_name = "zip_elasticsearch"
}



## NEED TO DEPLOY THIS  AFTER PUSHING THE IMAGES
resource "aws_ecs_task_definition" "go_server_task" {
  family             = "go_server"
  execution_role_arn = data.aws_iam_role.ecsTaskExecutionRole.arn
  container_definitions = jsonencode([
    {
      name    = "server"
      image   = data.aws_ecr_repository.zip_go_server.repository_url
      command = ["/usr/local/bin/wait-for-it.sh", "-h", data.aws_instance.ecs_main.private_ip, "-p", "9200", "-t", "30", "--", "/usr/local/bin/server", "--address", ":8181", "--es-server-url", format("http://%s:%d", data.aws_instance.ecs_main.private_ip, 9200)]

      cpu       = 10
      memory    = 128
      essential = true
      portMappings = [
        {
          containerPort = 8181
          hostPort      = 8181
        }
      ]

      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = data.aws_cloudwatch_log_group.ecs_log_group.name
          awslogs-region        = var.region
          awslogs-stream-prefix = "go_server"
        }
      }
    }
  ])
}

resource "aws_ecs_service" "go_server_service" {
  name            = "go_server"
  cluster         = data.aws_ecs_cluster.zip_elasticsearch.id
  task_definition = aws_ecs_task_definition.go_server_task.arn
  desired_count   = 1

  ordered_placement_strategy {
    type  = "binpack"
    field = "cpu"
  }
}

resource "aws_ecs_task_definition" "python_client_task" {
  family             = "python_client"
  execution_role_arn = data.aws_iam_role.ecsTaskExecutionRole.arn
  container_definitions = jsonencode([
    {
      name    = "client"
      image   = data.aws_ecr_repository.zip_python_client.repository_url
      command = ["./wait-for-it.sh", "-h", data.aws_instance.ecs_main.private_ip, "-p", "8181", "-t", "60", "--", "python", "client.py"]
      environment = [
        { name : "SERVER_ADDRESS", value : format("http://%s:%d", data.aws_instance.ecs_main.private_ip, 8181) },
        { name : "MAX_USER_IDS", value : "100" }
      ]
      cpu       = 10
      memory    = 128
      essential = true
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = data.aws_cloudwatch_log_group.ecs_log_group.name
          awslogs-region        = var.region
          awslogs-stream-prefix = "python_client"
        }
      }
    }
  ])
}

resource "aws_ecs_service" "python_client_service" {
  name            = "python_client"
  cluster         = data.aws_ecs_cluster.zip_elasticsearch.id
  task_definition = aws_ecs_task_definition.python_client_task.arn
  desired_count   = 1

  ordered_placement_strategy {
    type  = "binpack"
    field = "cpu"
  }
}

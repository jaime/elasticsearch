variable "lambda_function_name" {
  default = "python_lambda_query"
}

data "aws_ecr_repository" "zip_python_lambda" {
  name = "zip/lambda"
}

data "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"
}

resource "aws_default_subnet" "default_ap_southeast_2c" {
  availability_zone = "ap-southeast-2c"

  tags = {
    Name = "Default subnet for ap-southeast-2c"
  }
}


resource "aws_cloudwatch_log_group" "lambda_logging" {
  name              = "/aws/lambda/${var.lambda_function_name}"
  retention_in_days = 14
}

# See also the following AWS managed policy: AWSLambdaBasicExecutionRole
resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = data.aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

resource "aws_iam_role_policy_attachment" "AWSLambdaVPCAccessExecutionRole" {
  role       = data.aws_iam_role.iam_for_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_lambda_function" "python_lambda_query" {
  function_name = var.lambda_function_name
  package_type  = "Image"
  image_uri     = format("%s:latest", data.aws_ecr_repository.zip_python_lambda.repository_url)

  role = data.aws_iam_role.iam_for_lambda.arn

  vpc_config {
    security_group_ids = data.aws_instance.ecs_main.vpc_security_group_ids
    subnet_ids         = [data.aws_instance.ecs_main.subnet_id]
  }

  environment {
    variables = {
      ELASTICSEARCH_URL = format("http://%s:%d", data.aws_instance.ecs_main.private_ip, 9200)
    }
  }
  depends_on = [
    aws_iam_role_policy_attachment.lambda_logs,
    aws_iam_role_policy_attachment.AWSLambdaVPCAccessExecutionRole,
    aws_cloudwatch_log_group.lambda_logging,
  ]
}

resource "aws_cloudwatch_event_rule" "trigger_every_three_minutes" {
  name                = "trigger_every_three_minutes"
  description         = "Triggers an event every 3 minutes"
  schedule_expression = "rate(3 minutes)"
}

resource "aws_cloudwatch_event_target" "trigger_python_lambda_query" {
  rule      = aws_cloudwatch_event_rule.trigger_every_three_minutes.name
  target_id = "python_lambda_query"
  arn       = aws_lambda_function.python_lambda_query.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_python_lambda_query" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.python_lambda_query.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.trigger_every_three_minutes.arn
}

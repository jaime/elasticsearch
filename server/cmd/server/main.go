package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"gitlab.com/jaime/elasticsearch/server/internal/api"
	"gitlab.com/jaime/elasticsearch/server/internal/config"
	"gitlab.com/jaime/elasticsearch/server/internal/middleware"
	"gitlab.com/jaime/elasticsearch/server/internal/store/es"
)

func main() {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})
	logger.SetLevel(logrus.DebugLevel)
	log := logger.WithField("service", "api")

	cfg := config.New()

	router, err := setup(log, cfg)
	if err != nil {
		log.WithError(err).Fatal("failed to setup API")
	}

	log.Infof("http server listening: %s", cfg.Server.Addr)

	if err := http.ListenAndServe(cfg.Server.Addr, router); err != nil {
		log.WithError(err).Fatal("")
	}
}

func setup(log logrus.FieldLogger, cfg *config.Config) (*mux.Router, error) {
	store, err := es.New(log, cfg.ElasticServer.URL, cfg.ElasticServer.IndexName)
	if err != nil {
		return nil, err
	}

	apiHandlers := api.New(log, store, cfg.Server.OperationTimeout)

	r := mux.NewRouter()
	r.Use(middleware.LogRequest(log))

	r.MethodNotAllowedHandler = middleware.LogRequest(log)(http.HandlerFunc(apiHandlers.NotAllowed))
	r.NotFoundHandler = middleware.LogRequest(log)(http.HandlerFunc(apiHandlers.NotFound))

	r.NewRoute().
		PathPrefix("/healthcheck").
		HandlerFunc(apiHandlers.HealthCheck).
		Methods(http.MethodGet)

	r.NewRoute().
		PathPrefix("/api/v1/click").
		HandlerFunc(apiHandlers.Click).
		Queries("user_id", "{user_id}").
		Methods(http.MethodPut)

	return r, nil
}

# Elasticsearch

Elasticsearch tech challenge

## Instructions

We want to implement a Golang ingestion server on ECS that will receive http
requests representing click events from our users. The event is just composed of the
user_id. Those events will be ingested by our Elasticsearch database (also deployed
on ECS).

Every 3m we have a python lambda function to query the ES index to count what
happened during that elapsed time:
- the total number of events and
- the number of users

Create a python script that is sending fake events to your API.

BONUS: create an AWS cloudwatch log filter that will parse the logs and extract the
count metrics in order to create a monitoring dashboard.

You will use terraform to build the ECS/cloudwatch infrastructure on AWS and the
serverless function for the lambda function.


## Running locally

**NOTE**:
Required `docker` and `docker-compose` installed.
Tested with versions:`Docker version 20.10.7, build f0df350` and `docker-compose version 1.29.2, build 5becea4c`

A `docker-compose.yml` file is available at the root of this project.
To run all services execute the following command:

### Building and running

```shell
make up
# or specify ARGS to attach them to the command, for example to rebuild an image
make up ARGS='--build --force-recreate python-client'
```

An elasticsearch container will be run along with a `go-server`, `python-client` and `lambda`.

### Lambda test

The `lambda` container allows you to test the function execution as if it were called on AWS.
For example:

```shell
$ curl -s -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{}' | jq

{
  "status_code": 200,
  "total_users": 33,
  "total_clicks": 33,
  "from_ts": "2021-08-29T05:08:42.405523",
  "to_ts": "2021-08-29T05:11:42.405523"
}
```

### Logs

To follow the logs you can run:

```shell
make logs

# or for a specific service
make logs ARGS='go-server'
```


## Unit tests

Only Go unit tests are provided:

```shell
cd server
make test
```


## Deploying to AWS

Please ensure your credentials are set in `~/.aws/credentials` and the user has access to the following policies:

**NOTE**:
These policies are quite broad as they give full access to the resources. These could be tightened, but
I didn't want to spend too much time dealing with them.

- `AmazonEC2FullAccess`
- `IAMFullAccess`
- `AmazonEC2ContainerRegistryFullAccess`
- `CloudWatchFullAccess`
- `AmazonECS_FullAccess`
- `AWSCloudFormationFullAccess`
- `AWSLambda_FullAccess`
- `CloudWatchEventsFullAccess`

### How to deploy

1. Run the following commands to create the AWS resources like EC2 instance, IAM roles and ECR repos.

    ```shell
    cd infra
    terraform init
    terraform apply # and type yes to confirm the resources to be create
    ```

2. Once these are created, you'll need to build the Docker images and push them to ECR. You might need to login to the
container registry first.

   ```shell
   export AWS_ACCOUNT_ID=your-account-id
   export AWS_REGION=ap-southeast-2
   make docker-login
   make build-and-push-docker-images
   ```

3. Now you can create the ECS services, task definitions and the lambda function and trigger.
   ```shell
   cd infra/apps
   terraform init
   terraform apply # and type yes to confirm the resources to be create
   ```

4. Optionally, to destroy _ALL_ resources do:

   ```shell
   cd infra/apps
   terraform destroy -auto-approve
   cd ..
   terraform destroy -auto-approve
   ```

### Sample output from logs

Logs Insights query for Lambda function, server and client

```shell
fields event, response.status_code, response.total_clicks, response.total_users, timestamp
| filter event == "success"
| sort @timestamp desc
```

![log insights from lambda](img/log_insights_lambda.png)

```shell
fields time, msg, req_uri, res_status
| filter @logStream like 'go_server' and level = "info"
| sort @timestamp desc
| limit 20
```


![log insights from server](img/log_insights_server.png)

```shell
fields timestamp, event, user_id, status_code
| filter @logStream like 'client'
| sort @timestamp desc
| limit 20
```

![log insights from client](img/log_insights_client.png)

### Sample output for metrics

Total clicks in a 3-minute period.

![total clicks in a 3 minute period](img/total_clicks_sample.png)

Total unique users in a 3-minute period

![total unique users in a 3 minute period](img/total_users_sample.png)

## Trade-offs

- My terraform skills definitely require some improvements. For example, I'm not using environments
and not exporting variables. Most resources are defined in a single file, and I need to retrieve the `data` sources
inside `infra/apps/`.
- A bit of manual input is required to create the user allowed to perform the terraform actions (though I think that can't really be automated 🤔).
- There are no tests for infra, the client and the lambda function.
- The terraform state could be using a backend to store and handle it.
- I did not create a separate VPC nor subnets, so I'm relying on the default ones being there.
- I opened the security group manually for access from home for testing, but there is no real need to open ports to the internet
as the output can be seen in CloudWatch.
- There is no ELB/ALB in front of the ECS services, so the ports are hardcoded and there is no service discovery.
So I need to kill the container manually before a new version is deployed.
- I tried creating the metrics and I can see the data in the graphs but I'm not confident that's exactly what
I had to do.
- Since the client is running in a schedule every second, there are ~180 clicks in a 3 minute period.
- The total number of users is counted by using [search aggregations](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html). 
I could have used the [count API](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-count.html),
but I decided to keep the lambda function simple.

## GitLab CI

I used GitLab CI because I didn't want to waste time figuring out how GitHub Actions works.
If you'd like to see the pipelines in action, please share your GitLab username, and I can add you
to the project.

Here is a screenshot of a pipeline:

![pipeline sample](img/pipeline.png)

## About me

If you'd like to learn more about my work, you can see my contributions to GitLab in
[my profile](https://gitlab.com/jaime). I am a maintainer of
[GitLab Pages](https://gitlab.com/gitlab-org/gitlab-pages) and
GitLab's [release-cli](https://gitlab.com/gitlab-org/gitlab-pages)
so most of my contributions are done there :)

- [LinkedIn](https://www.linkedin.com/in/jaime-m88/)

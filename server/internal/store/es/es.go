package es

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/sirupsen/logrus"

	"gitlab.com/jaime/elasticsearch/server/internal/api"
)

// Document to be saved in ES
type Document struct {
	Timestamp time.Time `json:"timestamp"`
	User      *api.User `json:"user"`
}

// Store holds the ElasticSearch configuration and implements the api.Storage interface
type Store struct {
	log       logrus.FieldLogger
	client    *elasticsearch.Client
	indexName string
}

// New creates a new Store and initializes the users map
func New(log logrus.FieldLogger, serverURL, indexName string) (*Store, error) {
	client, err := elasticsearch.NewClient(elasticsearch.Config{
		Addresses: []string{serverURL},
	})
	if err != nil {
		return nil, err
	}

	res, err := client.Info()
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.IsError() {
		// nolint: goerr113 // IsError returns a string
		return nil, fmt.Errorf("elasticsearch info: %q", res.String())
	}

	fields := logrus.Fields{}
	if err := json.NewDecoder(res.Body).Decode(&fields); err != nil {
		return nil, err
	}

	fields["client_version"] = elasticsearch.Version

	log.WithFields(fields).Infof("connected to ElasticSearch successfully")

	return &Store{
		log:       log,
		client:    client,
		indexName: indexName,
	}, nil
}

// Save a userID into the Store
func (s *Store) Save(ctx context.Context, u *api.User) error {
	d := Document{
		Timestamp: time.Now(),
		User:      u,
	}

	doc, err := json.Marshal(d)
	if err != nil {
		return err
	}

	// Set up the request object.
	req := esapi.IndexRequest{
		Index:      s.indexName,
		DocumentID: strconv.FormatInt(time.Now().UnixNano(), 10),
		Body:       bytes.NewReader(doc),
		Refresh:    "true",
	}

	res, err := req.Do(ctx, s.client)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.IsError() {
		// nolint: goerr113 // IsError returns a string
		return fmt.Errorf("index document: %q", res.String())
	}
	// Deserialize the response into a map.
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		s.log.WithError(err).Warn("decode index response")

		return nil
	}

	s.log.Debugf("[%s] %s; version=%d", res.Status(), r["result"], int(r["_version"].(float64)))

	return nil
}

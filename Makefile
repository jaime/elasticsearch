.PHONY: up down logs

SERVER_PORT ?= 8181
SERVER_ADDRESS ?=  http://localhost:$(SERVER_PORT)
ES_SERVER_URL ?= http://elasticsearch:9200
AWS_ACCOUNT_ID ?=
AWS_REGION ?= ap-southeast-2
SERVER_TAG ?= latest
CLIENT_TAG ?= latest
LAMBDA_TAG ?= latest

up:
	docker-compose up -d ${ARGS}

down:
	docker-compose down --remove-orphans

logs:
	docker-compose logs -f ${ARGS}

docker-login:
	aws ecr get-login-password --region $(AWS_REGION) | docker login --username AWS --password-stdin $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com

build-and-push-docker-images:
	cd server && \
		docker build -t $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com/zip/server:$(SERVER_TAG) .
	docker push $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com/zip/server:$(SERVER_TAG)

	cd client && \
		docker build -t $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com/zip/client:$(CLIENT_TAG) .
	docker push $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com/zip/client:$(CLIENT_TAG)

	cd lambda && \
		docker build -t $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com/zip/lambda:$(LAMBDA_TAG) .
	docker push $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com/zip/lambda:$(LAMBDA_TAG)

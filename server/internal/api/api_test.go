package api_test

import (
	"context"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/jaime/elasticsearch/server/internal/api"
)

func TestHandleClick(t *testing.T) {
	logger, _ := test.NewNullLogger()

	tests := map[string]struct {
		timeout          time.Duration
		query            string
		store            *mockStore
		expectedStatus   int
		expectedResponse string
	}{
		"user_id is int": {
			query:          "?user_id=1",
			store:          &mockStore{},
			expectedStatus: http.StatusNoContent,
		},
		"user_id is not an int": {
			query:            "?user_id=string",
			store:            &mockStore{},
			expectedStatus:   http.StatusBadRequest,
			expectedResponse: "query param: user_id must be an integer\n",
		},
		"user_id is missing": {
			query:            "",
			store:            &mockStore{},
			expectedStatus:   http.StatusBadRequest,
			expectedResponse: "missing query param: user_id\n",
		},
		"store times out": {
			timeout:          time.Nanosecond,
			query:            "?user_id=1",
			store:            &mockStore{},
			expectedStatus:   http.StatusInternalServerError,
			expectedResponse: "something went wrong\n",
		},
		"store returns error": {
			query:            "?user_id=1",
			store:            &mockStore{err: errors.New("store failed")},
			expectedStatus:   http.StatusInternalServerError,
			expectedResponse: "something went wrong\n",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			timeout := time.Second
			if tt.timeout > 0 {
				timeout = tt.timeout
			}

			handlers := api.New(logger, tt.store, timeout)

			ww := httptest.NewRecorder()

			r := httptest.NewRequest(http.MethodPut, "/api/v1/click"+tt.query, nil)

			handlers.Click(ww, r)

			res := ww.Result()
			defer res.Body.Close()

			b, err := io.ReadAll(res.Body)
			require.NoError(t, err)

			assert.Equal(t, tt.expectedStatus, res.StatusCode)
			assert.Equal(t, tt.expectedResponse, string(b))
		})
	}
}

type mockStore struct {
	err error
}

func (ms *mockStore) Save(ctx context.Context, u *api.User) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
		return ms.err
	}
}

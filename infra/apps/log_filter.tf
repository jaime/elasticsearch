resource "aws_cloudwatch_log_metric_filter" "event_success_total_clicks" {
  name           = "TotalClicksInTheLastThreeMinutes"
  pattern        = "{ $.event = \"success\" }"
  log_group_name = aws_cloudwatch_log_group.lambda_logging.name

  metric_transformation {
    name      = "TotalClicks"
    namespace = "ElasticsearchClicks"
    value     = "$.response.total_clicks"
    default_value = "0"
    unit = "None"
  }
}

resource "aws_cloudwatch_log_metric_filter" "event_success_total_users" {
  name           = "TotalUsersInTheLastThreeMinutes"
  pattern        = "{ $.event = \"success\" }"
  log_group_name = aws_cloudwatch_log_group.lambda_logging.name

  metric_transformation {
    name      = "TotalUsers"
    namespace = "ElasticsearchClicks"
    value     = "$.response.total_users"
    default_value = "0"
    unit = "None"
  }
}

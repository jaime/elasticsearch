"""
simple lambda function that queries the Elasticsearch database
"""

import os
import datetime
from elasticsearch import Elasticsearch
import structlog
from structlog.processors import JSONRenderer, TimeStamper

log = structlog.get_logger(processors=[TimeStamper(fmt='iso'), JSONRenderer( sort_keys=True)])

def lambda_handler(event, context):
    # pylint: disable=unused-argument
    """
    query executes a search on the clicks index
    """

    es_client = Elasticsearch([os.getenv("ELASTICSEARCH_URL")])

    now = datetime.datetime.now()
    three_min_ago = now - datetime.timedelta(minutes=3)

    body = {
        "query": {
            "range":{
                "timestamp":{
                    "gte": three_min_ago,
                    "lte": now
                }
            }
        },
        "aggs": {
            "type_count": {
                "cardinality": {
                    "field": "user.id"
                }
            }
        },
        "_source": "false"
    }

    total_clicks =0
    total_users = 0

    try:
        res = es_client.search(index="clicks", body=body)
        total_clicks = res['hits']['total']['value']
        total_users = res['aggregations']['type_count']['value']
        # pylint: disable=broad-except
    except  Exception as exception:
        log.error("caught exception", error = exception)
        return { "status_code": 500 }

    response = {
        "status_code": 200,
        "total_users": total_users,
        "total_clicks": total_clicks,
        "from_ts": three_min_ago.isoformat(),
        "to_ts": now.isoformat()
    }

    log.info("success", response=response)

    return response

if __name__ == "__main__":
    lambda_handler([], [])

package config

import (
	"flag"
	"time"
)

var (
	address          = flag.String("address", ":8181", "http server address")
	esServerURL      = flag.String("es-server-url", "http://localhost:9200", "elasticsearch server URL")
	esIndexName      = flag.String("es-index-name", "clicks", "elasticsearch index name for clicks")
	operationTimeout = flag.Duration("api-op-timeout", 30*time.Second, "API operation timeout (e.g. save index)")
)

func initFlags() {
	flag.Parse()
}

module gitlab.com/jaime/elasticsearch/server

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.8.1
)

require (
	github.com/elastic/go-elasticsearch/v7 v7.14.0
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
